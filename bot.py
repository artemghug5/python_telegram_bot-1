import requests     #бібліотека запитів
import datetime      #Бібліотека lkz hj,jnb p lfnfvb
from config import bot_token, open_weather_token  #імпорт бот токена
from aiogram import Bot,types   #підключення бібліотеки з ботом і типами
from aiogram.dispatcher import Dispatcher    #Імпорт диспетчера бота
from aiogram.utils import  executor        #Імпорт викнавця бота


bot = Bot(token=bot_token) #бот
dp = Dispatcher(bot)      #диспетчер

@dp.message_handler(commands=["start"])
async  def start_command(message: types.Message):                                     #В цих рядках при запуску
    await message.answer("В якій точці земної кулі ви б хоітли дізнатись погоду ?")   #бота командою старт виводиться повідомлення
   

@dp.message_handler()
async def get_weather(message:types.Message):
                                                   #Список з смайликами
    smile_code = {
        "Clear": "Ясно \U00002600",
        "Clouds": "Хмарно \U00002601",
        "Rain": "Дощ \U00002614",
        "Drizzle": "Дощ \U00002614",
        "Thunderstorm": "Гроза \U000026A1",
        "Snow": "Сніг \U0001F328",
        "Mist": "Туман \U0001F32B",
    }

    try:
        r = requests.get(
            f"https://api.openweathermap.org/data/2.5/weather?q={message.text}&appid={open_weather_token}&units=metric" #погода по вказаному містуa
        )
        data = r.json()    #підключення до сайту
                                                  #Нижче йде діставання інформації з сайту
        town = data["name"]
        country = data["sys"]["country"]
        temp = data["main"]["temp"]
        weather_description = data["weather"][0]["main"]
        if weather_description in smile_code:
            wd = smile_code[weather_description]
        else:
            wd = "Незрозуміла погода"
        feels_like = data["main"]["feels_like"]
        humidity  = data["main"]["humidity"]
        wind = data["wind"]["speed"]
        sunrise_time = datetime.datetime.fromtimestamp(data["sys"]["sunrise"])
        sunset_time = datetime.datetime.fromtimestamp(data["sys"]["sunset"])
        lenght_day = datetime.datetime.fromtimestamp(data["sys"]["sunset"]) - datetime.datetime.fromtimestamp(data["sys"]["sunrise"])
                            #Вивід інформації на екран
        await message.answer(f"Станом на:{datetime.datetime.now().strftime('%Y-%m-%d %H:%M')}\n"
        f"Погода в {country}:{town}\n{wd}\nТемпература:{round(temp)}C°\n"
        f"Відчувається як{round(feels_like)}C°\nВологість:{humidity}%\n"
        f"Швидкість вітру:{wind}м/с\nСвітанок:{sunrise_time}\nЗахід сонця:{sunset_time}\n"
        f"Тривалість дня: {lenght_day}\nГарного і мирного дня\U0001F607")


                                     #Якщо допущена помилка
    except:
        await message.reply("\U00002620 Невірно вказані данні \U00002620")

if __name__ == '__main__':
    executor.start_polling(dp)